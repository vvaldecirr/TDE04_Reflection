package control;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Scanner;

public class Program {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		System.out.println("Entre com o nome da classe a ser instanciada: ");
		String nomeClasse = teclado.nextLine();
		
		try {
			Class objClass = Class.forName("model."+nomeClasse);
			Object novo = objClass.newInstance();

			System.out.println(novo.toString());

			for (Field f : objClass.getDeclaredFields()) {				
				String nomeAtr = f.getName();
				System.out.println("Entre com o valor do campo "+nomeAtr);
				
				String valor = teclado.nextLine();
				String nomeMetodo = "set"+Character.toUpperCase(nomeAtr.charAt(0))+nomeAtr.substring(1);
				
				Method metodoSet = objClass.getMethod(nomeMetodo, f.getType());
				
				if(f.getType() == String.class)
					metodoSet.invoke(novo, valor);
				else if(f.getType() == int.class)
					metodoSet.invoke(novo, Integer.parseInt(valor));

				System.out.println(novo.toString());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
